let labels = document.querySelectorAll('h5');
let addOnsPrice = document.querySelectorAll('.add-ons-price');

labels[0].style.backgroundColor = 'aqua';
labels[0].style.color = 'black';
cardsPrice = document.querySelectorAll('.cards p');
let monthlyPlan = true;
let selectedPlan = false;
let costArray= {};
let addOnLists = {};
let pagesData = document.querySelectorAll('.step-container');

document.addEventListener('click', (event) => {
    
    //check name email mob number validation
    if (event.target.id == '1st-page-next') {
        console.log(labels);
        let isInfoVerified = infoVerification();
        if (isInfoVerified) {
            changePage(pagesData, document.querySelector('.container-two'));
            changeLabelBackground(labels,  labels[1]);
        }
    }
    //check which plan is required monthly or yearly
    if (event.target.id == 'toggle') {
        changePlan(event);
    }
    //check plan level
    console.log(event.target);
    if (event.target.classList.contains('arcade-plan')) {
        makeBorderBlue(plans, document.querySelector('.arcade'));
        makePlanData('Arcade', event.target.dataset);
    }
    if (event.target.classList.contains('advanced-plan')) {
        makeBorderBlue(plans, document.querySelector('.advanced'));
        makePlanData('Advanced', event.target.dataset);
    }
    if (event.target.classList.contains('pro-plan')) {
        makeBorderBlue(plans, document.querySelector('.pro'));
        makePlanData('Pro', event.target.dataset); 
    }
    
    if (event.target.id == 'online-service') { //event.target.classList.contains('first-add-on')
       
        if(event.target.checked){
        document.querySelector('.online-s').style.border = '2px solid blueviolet';
        let cost = addOnsPrice[0].innerText;
        costArray['firstAddOn'] = Number(cost.replace(/\D/g, ''));
        addOnLists["first"] = [`Online Service`, addOnsPrice[0].innerText];
        }
        else{
        document.querySelector('.online-s').style.border = '2px solid grey';
        delete costArray.firstAddOn;
        delete addOnLists.first;
        }
    }
    if (event.target.id == 'larger-storage') {//event.target.classList.contains('second-add-on')
        if(event.target.checked){
            document.querySelector('.storage').style.border = '2px solid blueviolet';
            let cost = addOnsPrice[1].innerText;
            costArray['secAddOn'] = Number(cost.replace(/\D/g, ''));
            addOnLists["second"] = [`Larger storage`, addOnsPrice[1].innerText];
        }
        else{
            document.querySelector('.storage').style.border = '2px solid grey'; 
            delete costArray.secAddOn;
            delete addOnLists.second;
        }
    }
    if (event.target.id == 'customizable') {//event.target.classList.contains('third-add-on')
        if(event.target.checked){
            document.querySelector('.customized').style.border = '2px solid blueviolet';
            let cost = addOnsPrice[2].innerText;
            costArray['thirdAddOn'] = Number(cost.replace(/\D/g, ''));
            addOnLists["third"] = [`Customizable Profile`, addOnsPrice[2].innerText];
        }
        else{
            document.querySelector('.customized').style.border = '2px solid grey'; 
            delete costArray.thirdAddOn;
            delete addOnLists.third;
        }
    }
    if (event.target.id == '2nd-page-next') {
        if (selectedPlan) { 
            changePage(pagesData, document.querySelector('.container-three'));
            changeLabelBackground(labels,  labels[2]);
        }
        else{
            document.querySelector('#error-plan').style.display = 'block' 
        }

        document.querySelectorAll('.new-added').forEach((addon)=>{
            console.log(addon);
            addon.style.display='none';
        });
    }
    if (event.target.id == '2nd-page-back') {
    
        changePage(pagesData, document.querySelector('.container-one'));
        changeLabelBackground(labels,  labels[0]);

    }

    if (event.target.id == '3rd-page-next') {
        console.log(costArray);
        addTheAddOns();
        calculateTotal();

        changePage(pagesData, document.querySelector('.container-four'));
        changeLabelBackground(labels,  labels[3]);

    }
    if (event.target.id == '3rd-page-back') {
        changePage(pagesData, document.querySelector('.container-two'));
        changeLabelBackground(labels,  labels[1]);

    }

    if (event.target.id == '4th-page-next') {
        changePage(pagesData, document.querySelector('.container-five'));

    }
    if (event.target.id == '4th-page-back') {

        console.log();
        document.querySelectorAll('.new-added').forEach((addon)=>{
            console.log(addon);
            addon.style.display='none';
        })

        changePage(pagesData, document.querySelector('.container-three'));

        changeLabelBackground(labels,  labels[2]);

    }

    if(event.target.id == 'go-back'){
        
        changePage(pagesData, document.querySelector('.container-two'));
        changeLabelBackground(labels,  labels[1]);

        document.querySelectorAll('.checked').
            forEach((addon) => {
                addon.checked = false});
        document.querySelectorAll('.add-ons')
            .forEach((addon)=>{
                addon.style.border = '2px solid grey';
            });
        Object.keys(costArray).forEach((cost)=>{
            delete costArray[cost];
        })
        Object.keys(addOnLists).forEach((addon)=>{
            delete addOnLists[addon];
        })
    }
});

function infoVerification() {

    let name = document.getElementById('name').value;
    let email = document.getElementById('email').value;
    let mobNo = document.getElementById('number').value;
    let isError = false;
    if (name.length < 3) {
        document.querySelector('#name-error').innerText = 'Enter a valid name';
        isError = true;
    } else {
        document.querySelector('#name-error').innerText = '';
    }
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (emailPattern.test(email) == false) {
        document.querySelector('#email-error').innerText = 'Enter valid Email Address';
        isError = true;
    } else {
        document.querySelector('#email-error').innerText = '';
    }
    if (mobNo.length < 10 || mobNo.length>10) {
        document.querySelector('#number-error').innerText = 'Enter valid number';
        isError = true;
    } else {
        document.querySelector('#number-error').innerText = '';
    }
    if(isError){
        return false;
    }else{
       return true;
    }
    
}

cardsPrice = document.querySelectorAll('.cards p');
addOnsPrice = document.querySelectorAll('.add-ons-price');


function changePlan(event) {
    document.querySelectorAll('.card').forEach((card)=>{
            card.style.border = '2px solid gray';
    });
    selectedPlan = false;
    let duration = 'mo';
    monthlyPlan = true;
    let planeName = 'monthly';
    document.querySelectorAll('h6').forEach(item => { item.innerText = ''; });
    if (event.target.checked) {
        duration = 'yr'
        monthlyPlan = false;
        planeName = 'yearly';
        document.querySelectorAll('h6').forEach(item => { item.innerText = '2 months free'; });
    }
    cardsPrice.forEach((card)=>{
        card.innerText = `$${card.dataset[planeName]}/${duration}`;
    });
    addOnsPrice.forEach((addOn)=>{
        addOn.innerText = `$${addOn.dataset[planeName]}/${duration}`;
    });
}

function addTheAddOns() {
    Object.values(addOnLists).forEach((addon)=>{
        console.log(addon[0] , ' first');
        console.log(addon[1] , ' second');
        let addOnPlan = document.createElement('div');
        let data1 = document.createElement('p');
        let data2 = document.createElement('p');
        data1.innerText = addon[0];
        data2.innerText = addon[1];
        addOnPlan.appendChild(data1);
        addOnPlan.appendChild(data2);
        addOnPlan.classList = 'plans';
        addOnPlan.classList.add('new-added');
        document.querySelector('.selected-plan').appendChild(addOnPlan); 
    });
}

function calculateTotal(){
    let sum = Object.values(costArray).reduce((acc,data)=>{
        acc+=data;
        return acc;
    },0);
    if(monthlyPlan){
    document.querySelector('.total-cost')
        .innerHTML = `<p>Total (per month)</p> 
                    <h3>$ ${sum}/mo</h3>`;
    }
    else{
        document.querySelector('.total-cost')
        .innerHTML = `<p>Total (per year)</p> 
                    <h3>$ ${sum}/yr</h3>`;
    }
}

const plans = document.querySelectorAll('.card');
console.log(plans,'plans');

function makeBorderBlue(plans, currPlan){
    plans.forEach((plan)=>{
        plan.style.border = '2px solid gray';
    });
    currPlan.style.border = '2px solid blueviolet';
}

function changeLabelBackground(labels, currPlan){
    labels.forEach((label)=>{
        label.style.backgroundColor = 'unset';
        label.style.color = 'white';
    });
    currPlan.style.backgroundColor = 'aqua';
    currPlan.style.color = 'black';
}

function changePage(pagesData, currPage) {
    pagesData.forEach((page)=>{
        page.style.display = 'none';
    });
    currPage.style.display = 'flex';
}

function makePlanData(planName,price){
    document.querySelector('#error-plan').style.display = 'none'; 
    selectedPlan = true;

    if (monthlyPlan) {
        price = price.monthly;
        costArray['main-price']= Number(price);
        document.querySelector('#main-price').innerText = price;
        document.querySelector('#main-plan-name').innerText = `${planName} (monthly)`;
        document.querySelector('#main-price').innerText = `$${price}/mo`;
    }
    else {
        document.querySelector('#main-plan-name').innerText = `${planName} (Yearly)`;
        price =price.yearly
        costArray['main-price']= Number(price);
        document.querySelector('#main-price').innerText = `$${price}/yr`;
    }
}